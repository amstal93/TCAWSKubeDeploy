resource "aws_iam_role" "instance_role" {
  name = "instance_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "kube_instance_profile" {
  name = "kubernetes_instance_profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_iam_role_policy_attachment" "instance-attach" {
  role       = aws_iam_role.instance_role.name
  policy_arn = data.aws_iam_policy.instance_policy.arn
}