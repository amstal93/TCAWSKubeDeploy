#!/bin/bash

amazon-linux-extras install ansible2 -y
sudo yum -y install git

git clone https://github.com/ArunaLakmal/ansible-kube-master.git
ansible-playbook ansible-kube-master/install_packages.yaml --extra-vars "@ansible-kube-master/versions.json"