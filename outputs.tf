output "vpc_id" {
  value = aws_vpc.tc_vpc.id
}

output "public_subnet_id" {
  value = aws_subnet.tc_public1_subnet.id
}

output "private_subnet_id" {
  value = aws_subnet.tc_private1_subnet.id
}

output "kube_sg_id" {
  value = aws_security_group.tc_kubeadm_sg.id
}

output "key_pair" {
  value = aws_key_pair.tc_key.id
}